package me.gilak.Loop;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

import cz.msebera.android.httpclient.Header;


public class LoopModule extends ReactContextBaseJavaModule {

    private Promise postPromise;

    public LoopModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "Loop";
    }

    @ReactMethod
    public void post(String url, String jsonifiedData, final Promise promise) {
        postPromise = promise;

        try {
            JSONObject jsonObj = new JSONObject(jsonifiedData);
            JSONArray names = jsonObj.names();

            postRequest(url, parseData(jsonObj, names));
        } catch (JSONException e) {
            e.printStackTrace();
            postPromise.reject("failed", e.getMessage());
        }
    }


    protected RequestParams parseData(JSONObject data, JSONArray names) {
        RequestParams parameters = new RequestParams();
        boolean nextOneIsFile = false;
        File file;
        for (int i = 0; i < names.length(); i++) {
            try {
                if (!nextOneIsFile) {
                    parameters.put(names.getString(i), data.getString(names.getString(i)));
                } else {
                    try {
                        file = new File( new URI( data.getString( names.getString(i) ) ) );
                        parameters.put(names.getString(i), file);
                        nextOneIsFile = false;
                    } catch (FileNotFoundException | URISyntaxException e) {
                        e.printStackTrace();
                        postPromise.reject("failed", e.getMessage());
                    }
                }


                if (data.getString(names.getString(i)).equals("file")) {
                    nextOneIsFile = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return parameters;
    }

    protected void postRequest(String url, RequestParams params) {
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    postPromise.resolve(new String(responseBody,"UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    postPromise.reject("failed", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                postPromise.reject("failed", error.getMessage());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
}
