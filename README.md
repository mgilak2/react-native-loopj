#React Native Loopj Android Asynchronous Http Client

so fetch couldnt send my data with a file attached to a single request. 
I did simply port loopj to send my info over post request. 

you must read my java code before use it for now.
 I will develop more this package to make it more general in soon future.

###Usage

```js
NativeModules.Loop.post(url, data).then(...)     
```

###MainActivity.java

```java
package me.gilak.sample;

import android.content.Intent;


import me.gilak.Loop.LoopPackage;


import com.facebook.react.ReactActivity;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends ReactActivity {
    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    @Override
    protected String getJSBundleFile() {
        return CodePush.getBundleUrl();
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "EvandDiscovery";
    }

    /**
     * Returns whether dev mode should be enabled.
     * This enables e.g. the dev menu.
     */
    @Override
    protected boolean getUseDeveloperSupport() {
        return BuildConfig.DEBUG;
    }

    /**
     * A list of packages used by the app. If the app uses additional views
     * or modules besides the default ones, add more packages here.
     */
    @Override
    protected List<ReactPackage> getPackages() {
        return Arrays.<ReactPackage>asList(
                new LoopPackage()
        );
    }
}


```


#####Any contributions are welcome